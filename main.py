import numpy as np
import cv2


def LowV(file):
    img_src = cv2.imread(f"images/{file}")
    kernel = np.array([
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1]])
    kernel = kernel/(np.sum(kernel) if np.sum(kernel) != 0 else 1)
    img_rst = cv2.filter2D(img_src, -1, kernel)
    cv2.imwrite(f'results/{file}_L.jpg', img_rst)


def HighV(file):
    img_src = cv2.imread(f"images/{file}")
    kernel = np.array([
        [0.0, -1.0, 0.0],
        [-1.0, 4.0, -1.0],
        [0.0, -1.0, 0.0]])
    kernel = kernel / (np.sum(kernel) if np.sum(kernel) != 0 else 1)
    img_rst = cv2.filter2D(img_src, -1, kernel)
    cv2.imwrite(f'results/{file}_H.jpg', img_rst)


def HighVP(file):
    img_src = cv2.imread(f"images/{file}")
    kernel = np.array([
        [0.0, -1.0, 0.0],
        [-1.0, 5.0, -1.0],
        [0.0, -1.0, 0.0]])
    kernel = kernel / (np.sum(kernel) if np.sum(kernel) != 0 else 1)
    img_rst = cv2.filter2D(img_src, -1, kernel)
    cv2.imwrite(f'results/{file}_VP.jpg', img_rst)


def myrastr(file):
    img_src = cv2.imread(f"images/{file}")
    kernel = np.array([
        [0.0, -1.5, 0.0],
        [-1.5, 10.0, -1.5],
        [0.0, -1.5, 0.0]])
    kernel = kernel / (np.sum(kernel) if np.sum(kernel) != 0 else 1)
    img_rst = cv2.filter2D(img_src, -1, kernel)
    cv2.imwrite(f'results/{file}_my.jpg', img_rst)

myrastr("my.jpg")

